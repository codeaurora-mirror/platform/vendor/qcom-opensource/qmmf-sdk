cmake_minimum_required(VERSION 3.1)

project(qmmf_recorder_client)


if (NOT RECORDER_CLIENT_ENABLED)
  set(exclude EXCLUDE_FROM_ALL)
else()
  #EXPORT_HEADERS
  install(FILES
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder_params.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder_extra_param.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder_extra_param_tags.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_device.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_codec.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_buffer.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_avcodec_params.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder_common.h
    ${TOP_DIRECTORY}/include/qmmf-sdk/qmmf_recorder_interface.h
    DESTINATION include/qmmf-sdk)
endif()

add_library(qmmf_recorder_client SHARED ${exclude}
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder.cc
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder_client.cc
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder_client_ion.cc
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder_extra_param.cc
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder_common.cc
${CMAKE_CURRENT_SOURCE_DIR}/qmmf_recorder_interface.cc
)

target_include_directories(qmmf_recorder_client
 PRIVATE ${TOP_DIRECTORY})

target_include_directories(qmmf_recorder_client
 PRIVATE $<BUILD_INTERFACE:${KERNEL_INCDIR}/usr/include>)

target_include_directories(qmmf_recorder_client
 PRIVATE ${TOP_DIRECTORY}/common/memory)

# TODO remove this hack when camx issue with propagating c and cpp glags is solved
target_include_directories(qmmf_recorder_client
 PRIVATE ${PKG_CONFIG_SYSROOT_DIR}/usr/include/ion_headers)

install(TARGETS qmmf_recorder_client DESTINATION lib OPTIONAL)
target_link_libraries(qmmf_recorder_client log binder utils cutils dl
 camera_metadata gtest gtest_main qmmf_av_queue pthread)
if(NOT CAMERA_CLIENT_DISABLED)
target_link_libraries(qmmf_recorder_client camera_client)
endif()

# TODO remove this hack when camx issue with propagating c and cpp glags is solved
target_link_libraries(qmmf_recorder_client ion)

if(TARGET_USES_GBM)
target_link_libraries(qmmf_recorder_client gbm)
endif()
